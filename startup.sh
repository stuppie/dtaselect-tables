nohup venv/bin/celery worker -A server.celery --loglevel=info > celery.log 2>&1&
echo $! > celery.pid
nohup python server.py > server.log 2>&1&
echo $! > server.pid
