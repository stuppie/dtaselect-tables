## To setup:

### Creat virtualenv
```
> virtualenv venv
> source venv/bin/activate
```

### Install requirements
```
> pip install numpy
> pip install -r requirements.txt
```

## To Run

### Start up redis
```
> redis-server
```

### Start up Celery worker
```
> venv/bin/celery worker -A server.celery --loglevel=info
```
### Start up Flask
```
> python server.py
```

## Requirements
- python3 (not python2)
- redis
- https://bitbucket.org/sulab/metaproteomics accessible on your pythonpath