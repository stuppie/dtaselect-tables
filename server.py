"""
to run celery worker: venv/bin/celery worker -A server.celery --loglevel=info

"""
import json
import uuid
from itertools import chain

from metaproteomics.analysis import functional_analysis, taxonomy
from metaproteomics.analysis.DBInfo import DBInfo
from metaproteomics.file_processing import blazmass_tools
from metaproteomics.goatools import obo_parser

import os
from celery import Celery
from flask import Flask, request
from flask import render_template, jsonify
from werkzeug.utils import secure_filename

ontology = obo_parser.GODag()

app = Flask(__name__)
app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)
print(app)
print(celery)

UPLOAD_FOLDER = 'static/'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route("/dtaselect", methods=['GET', 'POST'])
def index():
    if request.method == "GET":
        return render_template('upload.html')

    if request.method == 'POST':
        file = request.files['file']
        print(request.form)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            unique_id = str(uuid.uuid4())
            dtaselect_filename = os.path.join(app.config['UPLOAD_FOLDER'], filename + "_" + unique_id)
            file.save(dtaselect_filename)  # save uploaded dtaselect file into static folder on server
            json_filename = os.path.join(app.config['UPLOAD_FOLDER'], unique_id + ".json")

            # This db info will be passed into the long task we will call
            # It will also be stored in the json file the long task produces
            db_preset = request.form['db_preset']
            # call task to convert dtaselect file into json + look up extra info
            db_info = DBInfo(db_preset)
            task = process_dtaselect.apply_async((dtaselect_filename, json_filename, db_info), queue="celery")
            return render_template('progress.html', task_id=task.id)


@app.route("/data/<file_id>")
def data(file_id):
    # Display a dtaselect table
    json_filename = os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(file_id) + ".json")
    with open(json_filename) as f:
        j = json.load(f)
        header = j['header']
        db_info = j['db_info']
    return render_template('datatable.html', json_filename=json_filename, header=header, db_info=db_info)


@app.route("/demo")
def demo():
    return data("demo")


@app.route("/clustertable/<file_id>")
def clustertable(file_id):
    json_filename = os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(file_id) + ".json")
    with open(json_filename) as f:
        j = json.load(f)
        samples = j['samples']
        # Fill in peptides field if it doesn't exists
        changed = False
        for cluster in j["data"]:
            if "peptides" not in cluster:
                cluster['peptides'] = ';' + ';'.join(
                    set(chain(*[x.keys() for x in cluster['cluster_peptides'].values()]))) + ';'
                changed = True
    if changed:
        with open(json_filename, 'w') as f:
            json.dump(j, f)
    return render_template("clustertable.html", json_filename=json_filename,
                           samples=samples, crapome='crapome' in j['data'][0],
                           Pfam_info=j.get('Pfam_info', None))


@app.route("/ratiotable/<file_id>")
def significancetabletable(file_id):
    json_filename = os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(file_id) + ".json")
    with open(json_filename) as f:
        j = json.load(f)
        samples = j['samples']
        # Fill in peptides field if it doesn't exists
        changed = False
        for cluster in j["data"]:
            if "peptides" not in cluster:
                cluster['peptides'] = ';' + ';'.join(
                    set(chain(*[x.keys() for x in cluster['cluster_peptides'].values()]))) + ';'
                changed = True
    if changed:
        with open(json_filename, 'w') as f:
            json.dump(j, f)
    return render_template("ratiotable.html", json_filename=json_filename,
                           samples=samples, crapome='crapome' in j['data'][0],
                           Pfam_info=j.get('Pfam_info', None))


def dta_select_to_json(in_file, db_info):
    """
    dtaselect-filter to json
    Plus looks up GO terms and taxonomy
    """
    protDB = db_info.protDB
    domainDB = db_info.domainDB
    hashDB = db_info.hashDB
    taxDB = db_info.taxDB

    f = functional_analysis.Functionizer(protDB=protDB, domainDB=domainDB, hashDB=hashDB)
    t = taxonomy.Taxonomy(host='wl-cmadmin.scripps.edu', port=27017)  # ncbi tax info, not changing
    parser = blazmass_tools.dta_select_parser(in_file, get_tax=True, taxDB=taxDB, protDB=protDB)
    for locus in parser:
        locus_row = locus.copy()
        locus_row['forward_loci'] = ', '.join(map(str, locus_row['forward_loci']))
        locus_row['numpep'] = len(locus['peptide_seq'])
        locus_row['other_names'] = [x['description'] for x in locus['loci']]
        go_list = f.get_annotations_from_protIDs(locus['forward_loci']).get('go', [])
        locus_row['go'] = [go + ": " + ontology[go].name for go in go_list] if go_list else []
        locus_row['lca_organism'] = t.taxid_to_taxonomy(locus_row['lca'])['scientific_name'] if locus_row['lca'] else ''
        # add a peptide column so we can search by peptides
        locus_row['peptide_str'] = ';' + ';'.join(set(locus_row['unmod_peptide_seq'] + locus_row['peptide_seq'])) + ';'
        yield locus_row


# https://github.com/miguelgrinberg/flask-celery-example
@celery.task(bind=True)
def process_dtaselect(self, dtaselect_filename, json_filename, db_info):
    total = len(list(blazmass_tools.dta_select_parser(dtaselect_filename, small=True)))
    rows = []
    for n, row in enumerate(dta_select_to_json(dtaselect_filename, db_info)):
        rows.append(row)
        self.update_state(state='PROGRESS', meta={'current': n, 'total': total})

    # parse header information from dtaselect-filter.txt file. Store in json dump
    header = blazmass_tools.dta_select_header(dtaselect_filename)
    json.dump({'data': rows, 'db_info': dict(db_info.items()), 'header': header}, open(json_filename, 'w'), indent=1)
    return {'current': n, 'total': total, 'status': 'DONE', 'result': os.path.basename(json_filename.rstrip(".json"))}


@app.route('/status/<task_id>')
def taskstatus(task_id):
    task = celery.AsyncResult(task_id)
    if task.state == 'PENDING':
        response = {
            'state': task.state,
            'current': 0,
            'total': 1,
            'status': 'Pending...'
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'current': task.info.get('current', 0),
            'total': task.info.get('total', 1),
            'status': task.info.get('status', '')
        }
        if 'result' in task.info:
            response['result'] = task.info['result']
    else:
        # something went wrong in the background job
        response = {
            'state': task.state,
            'current': 1,
            'total': 1,
            'status': str(task.info),  # this is the exception raised
        }
    return jsonify(response)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, debug=True)
