"""
minified version with only clustertable
"""
import json
import uuid
from itertools import chain

import os
from flask import Flask, request
from flask import render_template, jsonify
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = 'static/'
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route("/clustertable/<file_id>")
def clustertable(file_id):
    json_filename = os.path.join(app.config['UPLOAD_FOLDER'], secure_filename(file_id) + ".json")
    with open(json_filename) as f:
        j = json.load(f)
        samples = j['samples']
        # Fill in peptides field if it doesn't exists
        changed = False
        for cluster in j["data"]:
            if "peptides" not in cluster:
                cluster['peptides'] = ';' + ';'.join(
                    set(chain(*[x.keys() for x in cluster['cluster_peptides'].values()]))) + ';'
                changed = True
    if changed:
        with open(json_filename, 'w') as f:
            json.dump(j, f)
    return render_template("clustertable.html", json_filename=json_filename,
                           samples=samples, crapome='crapome' in j['data'][0],
                           Pfam_info=j.get('Pfam_info', None))


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8010, debug=True)
